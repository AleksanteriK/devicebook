import { Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ListDevices from './Listdevices';
import ListMyBookings from './Mybookings';

export default function Home (props) 
{
   /*This will be used in fetch requests*/
   var baseurl = "https://devicebooking-b6dca-default-rtdb.europe-west1.firebasedatabase.app";
   const [devices, setDevices] = useState([]);

   useEffect(() => 
   {
      const abortController = new AbortController();
    
      fetch(`${baseurl}/devices.json`, 
      {
         method: 'GET',
         headers: 
         {
           'Content-Type': 'application/json',
         },
      })
    
      .then(response => 
      {
         if (!response.ok) 
         {
            throw new Error('Network response was not ok');
         }

         return response.json();
      })
    
      .then(jsonData => 
      {
         setDevices(jsonData);
      })
    
      .catch(error =>
      {
         console.error('Error fetching data:', error);
      });
    
         return () => abortController.abort();
   }, []);

   return (
      <div>
         <header>
         <br></br>
         <Row className = 'align-items-center justify-content-center'>
            <Col className = 'text-center text-md-left'>
               <h3>Welcome!</h3>
            </Col>
         </Row>
         <br></br>
         </header>
         <Container>
            <Row>
               <Col>
                  <ListDevices baseurl = {baseurl} devices = {devices}/>
               </Col>
               <Col>
                  <ListMyBookings baseurl = {baseurl} devices = {devices}/>
               </Col>
            </Row>
         </Container>
      </div>
   )
}