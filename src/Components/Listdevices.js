import React, { useState } from 'react';
import { Row, Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function ListDevices ({baseurl, devices}) 
{   
    const [isLaptopsOpen, setLaptopsIsOpen] = useState(false);
    const [isDesktopsOpen, setDesktopsIsOpen] = useState(false);

    function ToggleLaptops () 
    {
        setLaptopsIsOpen((isLaptopsOpen) => !isLaptopsOpen);
    }

    function ToggleDesktops () 
    {
        setDesktopsIsOpen((isDesktopsOpen) => !isDesktopsOpen);
    }

    const RunAnywayLaptoplist = () => 
    {
        return (
            <ul id='laptopslist'>
                <h5>List of laptops</h5>
                {devices && devices.laptops && Object.values(devices.laptops.models).map((laptop, idx) => (
                    <li key={idx}>
                        {laptop.name}, Year: {laptop.year}
                    </li>
                ))}
            </ul>
        );
    };

    const RunAnywayDesktoplist = () => 
    {
        return (
            <ul id='desktopslist'>
                <h5>List of desktops</h5>
                {devices && devices.desktops && Object.values(devices.desktops.models).map((desktop, idx) => (
                    <li key={idx}>
                        {desktop.name}, Year: {desktop.year}
                    </li>
                ))}
            </ul>
        );
    };

    return (
    <Card className='list-card'>
        <button onClick={() => ToggleLaptops()}>
            {isLaptopsOpen ? 'Hide Laptops' : 'Show Laptops'}
        </button>
        <button onClick={() => ToggleDesktops()}>
            {isDesktopsOpen ? 'Hide Desktops' : 'Show Desktops'}
        </button>
        <Card>
        <Card className='list-card'>
            <Row>
                {isDesktopsOpen ? <RunAnywayDesktoplist/> : null}
            </Row>
            <Row>
                {isLaptopsOpen ? <RunAnywayLaptoplist/> : null}
            </Row>
        </Card>
        </Card>
    </Card>
    );
}
