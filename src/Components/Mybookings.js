import React, { useState } from 'react';
import { Row, Button, Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useForm } from 'react-hook-form';

export default function ListMyBookings ({baseurl, devices}) 
{
    const [selectedDesktop, setSelectedDesktop] = useState();
    const [selectedLaptop, setSelectedLaptop] = useState();
    const [isBookingsOpen, setBookingsOpen] = useState(false);
    const [bookingFailOrSuccess, setFailOrSuccess] = useState(null);
    const [bookingData, setBookingData] = useState();
    const {register, handleSubmit, formState: { errors }} = useForm();
    const onSubmit = (bookingdata) => Book(bookingdata);
    var modal = document.getElementById("popup");

    function ToggleBookings () 
    {
        setBookingsOpen((isBookingsOpen) => !isBookingsOpen);
    }

    function SetLaptop (selection) 
    {
        setSelectedLaptop(selection)
        setSelectedDesktop(null);
    }

    function SetDesktop (selection) 
    {
        setSelectedDesktop(selection)
        setSelectedLaptop(null);
    }

    const formatDate = (date) => 
    {
        const yyyy = date.getFullYear();
        const mm = String(date.getMonth() + 1).padStart(2, '0');
        const dd = String(date.getDate()).padStart(2, '0');
        return `${yyyy}-${mm}-${dd}`;
    };

    const now = formatDate(new Date());

    /*I would have otherwise added the incrementation of bookings amount to backend,
    but it would have required to upgrade Firebase so i'm doing it from the frontend side*/

    const UpdateBookings = async () => 
    {
        if (devices && devices.totalbookings) 
        {
            const updatedAmount = parseInt(devices.totalbookings.amount) + 1;
            devices.totalbookings.amount = updatedAmount;

            const payload = 
            {
                "amount": updatedAmount
            }
    
            try 
            {
                /*update the totalbookings amount*/
                const response = await fetch(`${baseurl}/devices/totalbookings.json`, 
                {
                    method: 'PATCH',
                    headers: 
                    {
                        'Content-Type': 'application/json',
                    },
                        body: JSON.stringify(payload),
                });
    
                if (!response.ok) 
                {
                    throw new Error("Failed to update total bookings amount");
                }
    
            } 
            
            catch (error) 
            {
                console.error("Error updating total bookings amount:", error);
            }
        }

        else 
        {
            console.log('Devices or totalbookings are not available yet.');
        }
    }

    const Book = async (bookingdata) => 
    {
        if (selectedLaptop === null && selectedDesktop !== null) 
        {
            const selectedModel = devices.desktops.models[selectedDesktop.toLowerCase()];

            /*Check if there are existing bookings for the selected desktop*/
            if (selectedModel && selectedModel.bookings) 
            {
                const existingBooking = Object.values(selectedModel.bookings).find(
                (booking) => booking.date === bookingdata.date
                );

                if (existingBooking) 
                {
                    setFailOrSuccess("There is already a booking for this desktop on the selected date! Choose another date or device")
                    return;
                }
            }

            /*Since i couldn't add any logic in the firebase, i create a new
            booking id in the frontend*/
            const newBookingId = `booking${devices.totalbookings.amount + 1}`;

            try 
            {    
                const payload = 
                {
                    bookingId: devices.totalbookings.amount + 1,
                    dame: bookingdata.name,
                    date: bookingdata.date,
                }

                const response = await fetch(`${baseurl}/devices/desktops/models/${selectedDesktop.toLowerCase()}/bookings/${newBookingId}.json`, 
                {
                    method: 'PUT',
                    headers: 
                    {
                        'Content-Type': 'application/json',
                    },
                        body: JSON.stringify(payload),
                });

                if (!response.ok) 
                {
                    throw new Error("Failed to get response from server");
                }

                setFailOrSuccess("Booking added successfully");

                if (modal) 
                {
                    modal.style.display = "block";
    
                        setBookingData(
                        {
                            name: bookingdata.name,
                            date: bookingdata.date,
                        });
                }

                await UpdateBookings();

            }

            catch (error)
            {
                setFailOrSuccess("Attempt to make the booking failed");
            }
        }

        else if (selectedLaptop !== null && selectedDesktop === null) 
        {   
            const selectedModel = devices.laptops.models[selectedLaptop.toLowerCase()];

            /*Check if there are existing bookings for the selected laptop*/
            if (selectedModel && selectedModel.bookings) 
            {
                const existingBooking = Object.values(selectedModel.bookings).find(
                (booking) => booking.date === bookingdata.date
                );

                if (existingBooking) 
                {
                    setFailOrSuccess("There is already a booking for this laptop on the selected date! Choose another date or device")
                    return;
                }
            }

            const newBookingId = `booking${devices.totalbookings.amount + 1}`;

            try 
            {    
                const payload = 
                {
                    bookingId: devices.totalbookings.amount + 1,
                    name: bookingdata.name,
                    date: bookingdata.date,
                }

                const response = await fetch(`${baseurl}/devices/laptops/models/${selectedLaptop.toLowerCase()}/bookings/${newBookingId}.json`, 
                {
                    method: 'PUT',
                    headers: 
                    {
                        'Content-Type': 'application/json',
                    },
                        body: JSON.stringify(payload),
                });

                if (!response.ok) 
                {
                    throw new Error("Failed to get response from server");
                }

                setFailOrSuccess("Booking added successfully");

                if (modal) 
                {
                    modal.style.display = "block";

                    setBookingData(
                    {
                        name: bookingdata.name,
                        date: bookingdata.date,
                    });
                }
                
                await UpdateBookings();
            }

            catch (error)
            {
                setFailOrSuccess("Attempt to make the booking failed");
            }
        }  

        else
        {
            console.error("No device selected!");
            setFailOrSuccess("No device was selected!");
        }

    }

    const SelectDevice = () => 
    {
        if (!devices || !devices.totalbookings) 
        {
            return null;
        }
    
        return (
            <div>
                <select onChange={(e) => SetLaptop(e.target.value)}>
                    <option value="">Book a Laptop</option>
                    {Object.values(devices.laptops.models).map((laptop) => (
                    <option key={laptop.name} value={laptop.name}>{laptop.name}</option>
                    ))}
                </select>
                <br/>
                <select onChange={(e) => SetDesktop(e.target.value)}>
                    <option value="">Book a Desktop</option>
                    {Object.values(devices.desktops.models).map((desktop) => (
                    <option key={desktop.name} value={desktop.name}>{desktop.name}</option>
                     ))}
                </select>
                <br/>
                {selectedLaptop ? (
                    <span>Selected Laptop: {selectedLaptop}</span>
                ) : <span>No laptop selected</span>}
                <br/>
                {selectedDesktop ? (
                    <span>Selected Desktop: {selectedDesktop}</span>
                ) : <span>No Desktop selected</span>}
            </div>
        );
    }

    const ShowTotalbookings = () => 
    {
        if (!devices || !devices.totalbookings) 
        {
            return null;
        }

        return (
            <span>
                Total amount of bookings: {devices.totalbookings.amount}
            </span>
        );
    }

    const closeModal = () => 
    {
        var modal = document.getElementById("popup");
  
        if (modal) 
        {
           modal.style.display = "none";
           setBookingData(null);
        }
    };

    return (
        <Card className = 'list-card'>
            <button className = 'list-bookings' onClick={() => ToggleBookings()}>
            My bookings
            </button>
        {isBookingsOpen ? 
        <Card>
            <ShowTotalbookings/>
        <Card className = 'list-card'>
            <Row>
                <span>Book a device</span>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input type="text" placeholder="Username" 
                    {...register('name', { required: true })}
                    />

                    <input
                    type = 'date'
                    id = 'date'
                    name = 'date'
                    min = {now}
                    {...register('date', { required: true })}
                    />

                    <Button type="submit">Make a Booking</Button>
                </form>
                <SelectDevice/>
            </Row>
            <span>{bookingFailOrSuccess}</span> 
        </Card>
        </Card>
        : null}
            {errors.name && <span>User required!</span>}
            {errors.date && <span>Date required!</span>}
            <div id="popup" className="modal">
                <div className="modal-content">
                    <span className="close" onClick={closeModal}>&times;</span>
                    {/* Render Booking details with booking data only when available */}
                    {bookingData && (
                    <div>
                        <h2>Booking Details</h2>
                        <p>Name: {bookingData.name}</p>
                        <p>Date: {bookingData.date}</p>
                        <div className="button no-print">
                            <Button onClick={() => window.print()}>Print Booking Information</Button>
                        </div>
                    </div>
                    )}
                </div>
            </div>
        </Card> 
    );
}
