import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // For additional matchers
import Home from '../Components/Home';

describe('Home Component', () => 
{
  test('renders welcome message', async () => {
    render(<Home />);
    expect(screen.getByText('Welcome!')).toBeInTheDocument();
  });

});
