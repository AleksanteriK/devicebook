import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // For additional matchers
import ListDevices from '../Components/Listdevices';

describe('ListDevices Component', () => 
{
        test('renders list of laptops and desktops when buttons are clicked', async () => {
        const devices = 
        {
                laptops: 
                {
                models: 
                {
                        laptop1: { name: 'Laptop 1', year: '2020' },
                        laptop2: { name: 'Laptop 2', year: '2021' },
                },
                },

        desktops: 
        {
                models: 
                {
                        desktop1: { name: 'Desktop 1', year: '2019' },
                        desktop2: { name: 'Desktop 2', year: '2020' },
                },
        },
        };

    render(<ListDevices devices={devices} />);

    //Assert that laptop and desktop lists are initially hidden
    expect(screen.queryByText('List of laptops')).not.toBeInTheDocument();
    expect(screen.queryByText('List of desktops')).not.toBeInTheDocument();

    //Click the "Show Laptops" button
    fireEvent.click(screen.getByText('Show Laptops'));

    //Assert that laptop list is visible
    expect(screen.getByText('List of laptops')).toBeInTheDocument();
    expect(screen.getByText('Laptop 1, Year: 2020')).toBeInTheDocument();
    expect(screen.getByText('Laptop 2, Year: 2021')).toBeInTheDocument();

    //Click the "Show Desktops" button
    fireEvent.click(screen.getByText('Show Desktops'));

    //Assert that desktop list is visible
    expect(screen.getByText('List of desktops')).toBeInTheDocument();
    expect(screen.getByText('Desktop 1, Year: 2019')).toBeInTheDocument();
    expect(screen.getByText('Desktop 2, Year: 2020')).toBeInTheDocument();

    //Click the "Hide Laptops" button
    fireEvent.click(screen.getByText('Hide Laptops'));

    //Assert that laptop list is hidden
    expect(screen.queryByText('List of laptops')).not.toBeInTheDocument();

    //Click the "Hide Desktops" button
    fireEvent.click(screen.getByText('Hide Desktops'));

    //Assert that desktop list is hidden
    expect(screen.queryByText('List of desktops')).not.toBeInTheDocument();
  });
});
