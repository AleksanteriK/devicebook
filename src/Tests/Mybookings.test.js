import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // For additional matchers
import ListMyBookings from '../Components/Mybookings';

describe('ListMyBookings Component', () => 
{
        test('renders booking form and modal correctly', async () => 
        {
            const baseurl = 'testBaseUrl';
            const devices = {
            laptops: 
            {
                models: 
                {
                    laptop1: { name: 'Laptop 1', year: '2020', bookings: {} },
                    laptop2: { name: 'Laptop 2', year: '2021', bookings: {} },
                },
            },

            desktops: 
            {
                models: 
                {
                    desktop1: { name: 'Desktop 1', year: '2019', bookings: {} },
                    desktop2: { name: 'Desktop 2', year: '2020', bookings: {} },
                },
            },

            totalbookings: 
            {
                amount: 0,
            },
        };

    render(<ListMyBookings baseurl={baseurl} devices={devices} />);

    //Assert that booking form is initially hidden
    expect(screen.queryByText('Book a device')).not.toBeInTheDocument();

    //Click the "My bookings" button
    fireEvent.click(screen.getByText('My bookings'));

    //Assert that booking form is visible
    expect(screen.getByText('Book a device')).toBeInTheDocument();

  });
});
